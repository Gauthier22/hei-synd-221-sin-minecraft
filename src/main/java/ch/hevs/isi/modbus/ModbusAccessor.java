package ch.hevs.isi.modbus;
import ch.hevs.isi.utils.Utility;
import com.serotonin.modbus4j.*;
import com.serotonin.modbus4j.code.DataType;
import com.serotonin.modbus4j.exception.ErrorResponseException;
import com.serotonin.modbus4j.exception.ModbusInitException;
import com.serotonin.modbus4j.exception.ModbusTransportException;
import com.serotonin.modbus4j.ip.IpParameters;
import com.serotonin.modbus4j.locator.BaseLocator;

/**
 * This class is a singleton who does the modbus communication
 */
public class ModbusAccessor {
    private static ModbusAccessor instance = null;
    private ModbusFactory modbusFactory = new ModbusFactory();
    private ModbusMaster master;
    private String host;
    private int port;


    /**
     * Constructor of ModbusAccessor
     */
    public ModbusAccessor(String host, int port) throws ModbusInitException {
        this.host = host;
        this.port = port;

        connect(true);
    }

    /**
     * This method return the instance of the modbus accessor with the basics host and port
     * @return instance of the modbusAccessor
     */
    public static ModbusAccessor getInstance() throws ModbusInitException {
        if (instance == null) {
            instance = new ModbusAccessor("localhost", 1502);
        }
        return instance;
    }

    /**
     * This method return the instance of the modbus accessor
     * @return instance of the modbusAccessor
     */
    public static ModbusAccessor getInstance(String host, int port) throws ModbusInitException {
        try{
            if (instance == null) {
                 instance = new ModbusAccessor(host, port);
            }
        }catch(ModbusInitException e){
            e.printStackTrace();
            System.out.println("Exception has been found for the method 'getInstance, in ModbusAccessor'");
        }
        return instance;
    }

    /**
     * This method set a new connection to the modbus
     *
     * @param "host" address to connect
     * @param "port" port to connect
     */
    public void connect(boolean state) throws ModbusInitException {
        IpParameters param = new IpParameters();
        param.setHost(host);
        param.setPort(port);
        /**
         * Create a new TCP master
         */
        master = modbusFactory.createTcpMaster(param, state);

        /**
         * Init the communication with the modbusMaster
         */
        try {
            master.init();
        } catch (ModbusInitException e) {
            e.printStackTrace();
            System.out.println("Exception has been found for the method 'connect, in ModbusAccessor'");
        }
    }

    /**
     * Method to read a boolean
     */
    public Boolean readBoolean(int regAddress) throws ModbusTransportException, ErrorResponseException {
        try {
            return master.getValue(BaseLocator.coilStatus(1, regAddress));
        } catch (ModbusTransportException | ErrorResponseException e) {
            e.printStackTrace();
            System.out.println("Exception has been found for the method 'readBoolean, in ModbusAccessor'");
        }
        return null;
    }

    /**
     * Method to write a boolean
     */
    public void writeBoolean(int regAddress, boolean newValue) {
        try {
            master.setValue(BaseLocator.coilStatus(1, regAddress), newValue);
        } catch (ModbusTransportException | ErrorResponseException e) {
            e.printStackTrace();
            System.out.println("Exception has been found for the method 'writeBoolean, in ModbusAccessor'");
        }
    }

    /**
     * Method to read a float
     */
    public Float readFloat(int regAddress) {
        try {
            return (float) master.getValue(BaseLocator.inputRegister(1, regAddress, DataType.FOUR_BYTE_FLOAT));
        } catch (ModbusTransportException | ErrorResponseException e) {
            e.printStackTrace();
            System.out.println("Exception has been found for the method 'readFloat, in ModbusAccessor'");
        }
        return null;
    }

    /**
     * Method to write a float
     */
    public void writeFloat(int regAddress, float newValue) {
        try {
            master.setValue(BaseLocator.holdingRegister(1, regAddress, DataType.FOUR_BYTE_FLOAT), newValue);
        } catch (ModbusTransportException | ErrorResponseException e) {
            e.printStackTrace();
            System.out.println("Exception has been found for the method 'writeFloat, in ModbusAccessor'");
        }
    }


    public static void main(String[] args) throws ModbusInitException, ErrorResponseException, ModbusTransportException {
        ModbusAccessor mbAccess = ModbusAccessor.getInstance("localhost", 1502);
        boolean running = true;
        while (running) {
            /**
             * Read float address 205
             */
            Float remoteFactorySPRead = mbAccess.readFloat(205);
            if (remoteFactorySPRead == null) {
                Utility.DEBUG("ModbusAccessor", "main", "Modbus connection error");
                running = false;
            } else {
                Utility.DEBUG("ModbusAccessor", "main", "Register 205: " + remoteFactorySPRead);
                Utility.waitSomeTime(1000);
            }

            /**
             * Write float address 205
             */
            mbAccess.writeFloat(205, 0.35f);

            /**
             * Read boolean address 401
             */
            Boolean remoteSloarSWRead = mbAccess.readBoolean(609);
            if (remoteFactorySPRead == null) {
                Utility.DEBUG("ModbusAccessor", "main", "Modbus connection error");
                running = false;
            } else {
                Utility.DEBUG("ModbusAccessor", "main", "Register 401: " + remoteSloarSWRead);
                Utility.waitSomeTime(1000);
            }

            /**
             * Write boolean address 401
             */
            mbAccess.writeBoolean(401, !remoteSloarSWRead);

        }

    }
}
