package ch.hevs.isi;

import ch.hevs.isi.core.BinaryDataPoint;
import ch.hevs.isi.core.DataPoint;
import ch.hevs.isi.core.FloatDataPoint;
import ch.hevs.isi.utils.Utility;
import com.serotonin.modbus4j.exception.ErrorResponseException;
import com.serotonin.modbus4j.exception.ModbusInitException;
import com.serotonin.modbus4j.exception.ModbusTransportException;

import java.io.IOException;

/**
 * to control the factory
 *
 */

public class MinecraftController {
    public static boolean USE_MODBUS4J = false;

    public static void usage() {
        System.out.println("Parameters: <InfluxDB Server> <Group Name> <MordbusTCP Server> <modbus TCP port> [-modbus4j]");
        System.exit(1);
    }

    public static void main(String[] args) throws IOException, ModbusTransportException, ErrorResponseException, ModbusInitException, NullPointerException {

        String dbHostName    = "localhost";
        String dbName        = "labo";
        String dbUserName    = "root";
        String dbPassword    = "root";

        String modbusTcpHost = "localhost";
        int    modbusTcpPort = 1502;

        // Check the number of arguments and show usage message if the number does not match.
        String[] parameters = null;

        // If there is only one number given as parameter, construct the parameters according the group number.
        if (args.length == 4 || args.length == 5) {
            parameters = args;

            // Decode parameters for influxDB
            dbHostName  = parameters[0];
            dbUserName  = parameters[1];
            dbName      = dbUserName;
            dbPassword  = Utility.md5sum(dbUserName);

            // Decode parameters for Modbus TCP
            modbusTcpHost = parameters[2];
            modbusTcpPort = Integer.parseInt(parameters[3]);

            if (args.length == 5) {
                USE_MODBUS4J = (parameters[4].compareToIgnoreCase("-modbus4j") == 0);
            }
        } else {
            usage();
        }


        // for testing

        BinaryDataPoint bdp = new BinaryDataPoint("testBinary", true);
        FloatDataPoint fdp = new FloatDataPoint("testFloat", true,1);
        System.out.println("bdp default value = " + bdp.getValue() );
        bdp.setValue(true);

        System.out.println("bdp " + bdp.getLabel() + " value = " + bdp.getValue());
        bdp.setValue(false);
        System.out.println("bdp " + bdp.getLabel() + " value = " + bdp.getValue());
        System.out.println("fdp default value = " + fdp.getValue());
        fdp.setValue(0.45f);
        System.out.println("fdp " + fdp.getLabel() + " value = " + fdp.getValue());
        fdp.setValue(0.75f);
        System.out.println("fdp " + fdp.getLabel() + " value = " + fdp.getValue());

        DataPoint dp = DataPoint.getDataPointFromLabel("testBinary");
        System.out.println("dp label = " + dp.getLabel());
        if (dp instanceof BinaryDataPoint) {
            System.out.println("dp " + dp.getLabel() + " value = " + "This value is totally undefined by SN!");
        }
        dp = DataPoint.getDataPointFromLabel("testFloat");
        System.out.println("dp label = " + dp.getLabel());
        if (dp instanceof FloatDataPoint) {
            System.out.println("dp " + dp.getLabel() + " value = " +((FloatDataPoint) dp).getValue());
        }
    }
}
