package ch.hevs.isi.SmartControll;

import ch.hevs.isi.core.BinaryDataPoint;
import ch.hevs.isi.core.FloatDataPoint;

import java.io.IOException;
import java.util.TimerTask;

public class SmartControll extends TimerTask {

    FloatDataPoint factoryPower = (FloatDataPoint) FloatDataPoint.getDataPointFromLabel("REMOTE_FACTORY_SP");
    FloatDataPoint homePower = (FloatDataPoint) FloatDataPoint.getDataPointFromLabel("HOME_P_FLOAT");
    FloatDataPoint publicPower = (FloatDataPoint) FloatDataPoint.getDataPointFromLabel("PUBLIC_P_FLOAT");
    FloatDataPoint bunkerPower = (FloatDataPoint) FloatDataPoint.getDataPointFromLabel("BUNKER_P_FLOAT");
    FloatDataPoint solarPower = (FloatDataPoint) FloatDataPoint.getDataPointFromLabel("SOLAR_P_FLOAT");
    FloatDataPoint windPower = (FloatDataPoint) FloatDataPoint.getDataPointFromLabel("WIND_P_FLOAT");
    FloatDataPoint coalPower = (FloatDataPoint) FloatDataPoint.getDataPointFromLabel("COAL_P_FLOAT");
    FloatDataPoint gridVoltage = (FloatDataPoint) FloatDataPoint.getDataPointFromLabel("GRID_U_FLOAT");
    BinaryDataPoint remoteSolar = (BinaryDataPoint) BinaryDataPoint.getDataPointFromLabel("REMOTE_SOLAR_SW");
    BinaryDataPoint remoteWind = (BinaryDataPoint) BinaryDataPoint.getDataPointFromLabel("REMOTE_WIND_SW");
    FloatDataPoint coalSetUp = (FloatDataPoint) FloatDataPoint.getDataPointFromLabel("REMOTE_COAL_SP");
    FloatDataPoint battLevel = (FloatDataPoint) FloatDataPoint.getDataPointFromLabel("BATT_CHRG_FLOAT");
    FloatDataPoint coalAmount = (FloatDataPoint) FloatDataPoint.getDataPointFromLabel("COAL_AMOUNT");

    /**
     * the run method make all the step for the optimisation of our minecraft factory
     */
    @Override
    public void run() {
        boolean limit = false;

        try {
            if(coalAmount.getValue() < 0.05f){
                limit = true;
                System.out.println("Coal amount is under 25%, be careful and fill it!");
            }

            if (battLevel.getValue() >= 0.99f) {
                remoteSolar.setValue(false);
                remoteWind.setValue(false);
                coalSetUp.setValue(0);
                factoryPower.setValue(1);
            } else if (battLevel.getValue() > 0.75f && battLevel.getValue() < 0.99f) {
                remoteSolar.setValue(true);
                remoteWind.setValue(true);
                coalSetUp.setValue(0);
                factoryPower.setValue(1);
            } else if (battLevel.getValue() > 0.5f && battLevel.getValue() <= 0.75f) {
                remoteSolar.setValue(true);
                remoteWind.setValue(true);
                if(limit == false){
                    if (solarPower.getValue() + windPower.getValue() > 500f){
                        coalSetUp.setValue(0);
                    }else{


                        coalSetUp.setValue(0.2f);
                    }
                }
                factoryPower.setValue(0.55f);
            } else if (battLevel.getValue() > 0.30f && battLevel.getValue() <= 0.5f) {
                remoteSolar.setValue(true);
                remoteWind.setValue(true);
                if(limit == false) {
                    if(limit == false) {
                        if (solarPower.getValue() + windPower.getValue() > 500f) {
                            coalSetUp.setValue(0.2f);
                        } else {
                            coalSetUp.setValue(0.5f);
                        }
                    }
                }
                factoryPower.setValue(0.25f);
            } else {
                remoteSolar.setValue(true);
                remoteWind.setValue(true);
                if(limit == false){
                    if (solarPower.getValue() + windPower.getValue() > 500f) {
                        coalSetUp.setValue(0.1f);
                    } else {
                        coalSetUp.setValue(1f);
                    }
                }
                factoryPower.setValue(0);
            }



        } catch (IOException e) {
            e.printStackTrace();
        }
    }


}
