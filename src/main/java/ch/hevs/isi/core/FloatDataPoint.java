package ch.hevs.isi.core;

import ch.hevs.isi.database.DatabaseConntector;
import ch.hevs.isi.field.FieldConnector;
import ch.hevs.isi.web.WebConnector;
import com.serotonin.modbus4j.exception.ErrorResponseException;
import com.serotonin.modbus4j.exception.ModbusInitException;
import com.serotonin.modbus4j.exception.ModbusTransportException;

import java.io.IOException;

/**
 * This class modify the DataPoint class to work with float
 */
public class FloatDataPoint extends DataPoint {
    private float value;
    private int range;

    /**
     * Constructor of FloatDataPoint
     * @param label Label of the datapoint
     * @param isOutput Type of the datapoint (input or output)
     */
    public FloatDataPoint(String label, boolean isOutput, int range){
        super(label, isOutput);
        this.range = range;
    }

    /**
     * Update the values on the three connectors
     * @param value New value of the datapoint
     */
    public void setValue(float value) throws IOException {
            this.value = value;
             DatabaseConntector.getInstance().onNewValue(this);
             WebConnector.getInstance().onNewValue(this);
             if (isOutput())
             FieldConnector.getInstance().onNewValue(this);
    }

    /**
     * Return the value of the FloatDataPoint
     */
    public float getValue(){
        return value;
    }
}
