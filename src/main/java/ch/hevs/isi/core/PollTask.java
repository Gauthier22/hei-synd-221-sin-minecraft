package ch.hevs.isi.core;
import ch.hevs.isi.core.*;
import com.serotonin.modbus4j.exception.ErrorResponseException;
import com.serotonin.modbus4j.exception.ModbusInitException;
import com.serotonin.modbus4j.exception.ModbusTransportException;

import java.util.Timer;
import java.util.TimerTask;

/**
 * This method manage the PollTask class
 */
public class PollTask extends TimerTask{

    /**
     * This method make the polling of all database
     */
    @Override
    public void run()  {
        BooleanRegister.poll();
        FloatRegister.poll();
    }
}
