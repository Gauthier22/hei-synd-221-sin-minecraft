package ch.hevs.isi.core;


import ch.hevs.isi.modbus.ModbusAccessor;
import com.serotonin.modbus4j.exception.ErrorResponseException;
import com.serotonin.modbus4j.exception.ModbusInitException;
import com.serotonin.modbus4j.exception.ModbusTransportException;

import java.io.IOException;
import java.util.HashMap;

/**
 * This class manage the boolean register
 */
public class BooleanRegister {
    private boolean value;
    private int address;
    private BinaryDataPoint bdp;

    private static HashMap<DataPoint, BooleanRegister> hm = new HashMap<>();

    /**
     * Constructor of BooleanRegister
     * @param Label    Label of the register
     * @param isOutput Type of the register (input or output)
     */
    public BooleanRegister(String Label, boolean isOutput, int address) {
        this.address = address;
        bdp = new BinaryDataPoint(Label, isOutput);
        hm.put(bdp, this);
    }

    /**
     * This class read the value on the register
     */
    public void read() throws ErrorResponseException, ModbusTransportException, IOException, ModbusInitException {
            boolean newValue = ModbusAccessor.getInstance().readBoolean(address);
            bdp.setValue(value);

    }

    /**
     * This class write a value on the register
     */
    public void write() throws ModbusInitException, ModbusTransportException, ErrorResponseException {
        try {
            boolean value = bdp.getValue();
            ModbusAccessor.getInstance().writeBoolean(address, value);
        }catch(ModbusInitException e){
            e.printStackTrace();
            System.out.println("Exception has been found for the method 'write', in BooleanRegister'");
        }
    }

    /**
     *
     */
    public static void poll() {
        try {
            // Loop which read() all values of hashmap
            for (BooleanRegister br : hm.values()) {
                br.read();
            }
        }catch(IOException | ModbusTransportException| ErrorResponseException| ModbusInitException e){
            e.printStackTrace();
            System.out.println("Exception has been found for the method 'poll', in BooleanRegister'");
        }
    }

    /**
     * @param fdp
     * @return a FloatRegister
     */
    public static BooleanRegister getRegisterFromDataPoint(BinaryDataPoint fdp) {
            BooleanRegister br = hm.get(fdp);
        return br;
    }
}


