package ch.hevs.isi.core;

import com.serotonin.modbus4j.exception.ModbusInitException;

import java.io.IOException;

/**
 * For the read
 */
public interface DataPointListener {

    public void onNewValue(BinaryDataPoint bdp) throws IOException; //C'était DataPoint

    void onNewValue(FloatDataPoint fdp) throws IOException, ModbusInitException;
}
