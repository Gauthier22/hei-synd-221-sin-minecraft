package ch.hevs.isi.core;

import ch.hevs.isi.modbus.ModbusAccessor;
import com.serotonin.modbus4j.exception.ModbusInitException;

import java.io.IOException;
import java.util.HashMap;

/**
 * This class manage the float register
 */
public class FloatRegister {
    int address;
    private FloatDataPoint fdp;
    int range;

    private static HashMap<DataPoint,FloatRegister > hm = new HashMap<>();

    /**
     * Constructor of FloatRegister
     * @param Label Label of the register
     * @param isOutput Type of the register (input or output)
     */
    public FloatRegister(String Label, boolean isOutput, int address, int range, int offset){
        this.address = address;
        this.range = range;
        fdp = new FloatDataPoint(Label, isOutput, range);
        hm.put(fdp, this);
    }
    /**
     * This class read the value of the register
     */
    public void read() throws ModbusInitException, IOException {
            float newValue = ModbusAccessor.getInstance().readFloat(address);
            fdp.setValue(newValue);
    }

    /**
     * This class write a value on the register
     */
    public void write () throws ModbusInitException {
        try {
            float value = fdp.getValue();
            ModbusAccessor.getInstance().writeFloat(address, value);
        }catch(ModbusInitException e){
            e.printStackTrace();
            System.out.println("Exception has been found for the method 'write', in FloatRegister'");
        }
    }

    /**
     *
     */
    public static void poll(){
        try {
            // Loop which read() all values of hashmap
            for (FloatRegister fr : hm.values()) {
                fr.read();
            }
        }catch(IOException | ModbusInitException e){
            e.printStackTrace();
            System.out.println("Exception has been found for the method 'poll', in FloatRegister'");
        }
    }

    /**
     * @param fdp
     * @return a FloatRegister
     */
    public static FloatRegister getRegisterFromDataPoint(FloatDataPoint fdp) {
        FloatRegister fr = hm.get(fdp);
        return fr;
    }

}
