package ch.hevs.isi.core;

import ch.hevs.isi.database.DatabaseConntector;
import ch.hevs.isi.field.FieldConnector;
import ch.hevs.isi.web.WebConnector;
import com.serotonin.modbus4j.exception.ErrorResponseException;
import com.serotonin.modbus4j.exception.ModbusInitException;
import com.serotonin.modbus4j.exception.ModbusTransportException;

import java.io.IOException;
import java.util.HashMap;

/**
 * This class create a datapoint who associate a value to a label
 */
public abstract class DataPoint {
    private static HashMap<String,DataPoint> hm = new HashMap<>();
    private String label;
    private boolean isOutput;

    /**
     * Constructor of DataPoint
     * @param label Label of the datapoint
     * @param isOutput Type of the datapoint
     */
    protected DataPoint(String label, boolean isOutput){
        this.label = label;
        this.isOutput = isOutput;
        hm.put(label, this);
    }

    /**
     * Return a datapoint from the research label on the maps
     */
    public static DataPoint getDataPointFromLabel (String label){
        return hm.get(label);
    }

    /**
     * Return the label of the DataPoint
     */
    public String getLabel(){
        return label;
    }

    /**
     * Specify if it's an input or an output
     */
    public boolean isOutput(){
        return isOutput;
    }

/*
    protected void publish() throws IOException, ModbusTransportException, ErrorResponseException, ModbusInitException {
        try {
            //Singleton
            DatabaseConntector dbc = DatabaseConntector.getInstance();
            WebConnector wbc = WebConnector.getInstance();

            // while the input here are the output from The field so the electrical age word
            if (this.isOutput) {
                FieldConnector fc = FieldConnector.getInstance();
                fc.onNewValue((BinaryDataPoint) this);
            }
            dbc.onNewValue(this);
            wbc.onNewValue(this);
        }catch(ModbusTransportException| ErrorResponseException| ModbusInitException e){
            e.printStackTrace();
            System.out.println("Exception has been found for the method 'publish', in DataPoint'");
        }

    }
    */

}
