package ch.hevs.isi.core;

import ch.hevs.isi.SmartControll.SmartControll;
import ch.hevs.isi.modbus.ModbusAccessor;
import ch.hevs.isi.web.WebConnector;
import com.serotonin.modbus4j.exception.ModbusInitException;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Timer;

/**
 * Test of the functionality of the core
 */
public class Launcher {
    /**
     * This class test of the functionality of the core
     */
    public static void main(String[] args) throws ModbusInitException {

        ModbusAccessor Ma = new ModbusAccessor("localhost", 1502);
        Ma.connect(true);

        WebConnector wb = WebConnector.getInstance();
        String line = "";
        String separateur = ";";

        /*
        //Declaration of DataPoint variable
        FloatDataPoint Try = new FloatDataPoint(14.8f, "Try", true);
        BooleanDataPoint Try1 = new BooleanDataPoint(false, "Try1", true);

        //Get the value of the float variable and the boolean variable
        Try.getValue();
        Try1.getValue();

        //Search the reference to the float DataPoint
        FloatDataPoint t = (FloatDataPoint) DataPoint.getDataPointFromLabel("Try");
        t.setValue(333f);

        //Search the reference to the boolean DataPoint
        BooleanDataPoint t1 = (BooleanDataPoint) DataPoint.getDataPointFromLabel("Try1");
        t1.setValue(false);
        */

        /**
         * Instantiate some FloatRegister and BooleanRegister objects
         * according to the SIn world specification and test polling

        FloatRegister GRID_U_FLOAT = new FloatRegister("GRID_U_FLOAT", false, 89, 1000,0);
        FloatRegister BATT_P_FLOAT = new FloatRegister("BATT_P_FLOAT", false, 57, 6000, -3000);
        FloatRegister BATT_CHRG_FLOAT = new FloatRegister("BATT_CHRG_FLOAT", false, 49, 1,0);
        FloatRegister SOLAR_P_FLOAT = new FloatRegister("SOLAR_P_FLOAT", false, 61, 1500,0);
        FloatRegister WIND_P_FLOAT = new FloatRegister("WIND_P_FLOAT", false, 53, 1000,0);
        FloatRegister COAL_P_FLOAT = new FloatRegister("COAL_P_FLOAT", false, 81, 600,0);
        FloatRegister COAL_AMOUNT = new FloatRegister("COAL_AMOUNT", false, 65, 1,0);
        FloatRegister HOME_P_FLOAT = new FloatRegister("HOME_P_FLOAT", false, 101, 1000,0);
        FloatRegister PUBLIC_P_FLOAT = new FloatRegister("PUBLIC_P_FLOAT", false, 97, 500,0);
        FloatRegister FACTORY_P_FLOAT = new FloatRegister("FACTORY_P_FLOAT", false, 105, 2000,0);
        FloatRegister BUNKER_P_FLOAT = new FloatRegister("BUNKER_P_FLOAT", false, 93, 500,0);
        FloatRegister WIND_FLOAT = new FloatRegister("WIND_FLOAT", false, 301, 1,0);
        FloatRegister WEATHER_FLOAT = new FloatRegister("WEATHER_FLOAT", false, 305, 1,0);

        FloatRegister WEATHER_FORECAST_FLOAT = new FloatRegister("WEATHER_FORECAST_FLOAT", false, 309, 1,0);
        FloatRegister WEATHER_COUNTDOWN_FLOAT = new FloatRegister("WEATHER_COUNTDOWN_FLOAT", false, 313, 600,0);
        FloatRegister CLOCK_FLOAT = new FloatRegister("CLOCK_FLOAT", false, 317, 1,0);
        FloatRegister REMOTE_COAL_SP = new FloatRegister("REMOTE_COAL_SP", true, 209, 1,0);
        FloatRegister REMOTE_FACTORY_SP = new FloatRegister("REMOTE_FACTORY_SP", true, 205, 1,0);
        BooleanRegister REMOTE_SOLAR_SW = new BooleanRegister("REMOTE_SOLAR_SW", true, 401);
        BooleanRegister REMOTE_WIND_SW = new BooleanRegister("REMOTE_WIND_SW", true, 405);
        FloatRegister FACTORY_ENERGY = new FloatRegister("FACTORY_ENERGY", false, 341, 3600000,0);
        FloatRegister SCORE = new FloatRegister("SCORE", false, 345, 3600000,0);
        FloatRegister COAL_SP = new FloatRegister("COAL_SP", false, 601, 1,0);
        FloatRegister FACTORY_SP = new FloatRegister("FACTORY_SP", false, 605, 1,0);
        BooleanRegister SOLAR_CONNECT_SW = new BooleanRegister("SOLAR_CONNECT_SW", false, 609);
        BooleanRegister WIND_CONNECT_SW = new BooleanRegister("WIND_CONNECT_SW", false, 613);

         */


        try{
            BufferedReader br = new BufferedReader(new FileReader("ModbusEA_SIn.csv"));
            br.readLine();

            while((line = br.readLine())!= null){
                String [] tab = line.split(separateur);
            System.out.println("Label = " + tab[0] + "boolean = " + tab[1] + "isOutput" + tab[2] + "Address " + tab[3] + "Range = " + tab[4] + "Offset = " + tab[5]);
                if(tab[1].equalsIgnoreCase("true")){
                    BooleanRegister booleanR = new BooleanRegister(tab[0], Boolean.parseBoolean(tab[2]), Integer.parseInt(tab[3]));
                }else{
                    FloatRegister floatR = new FloatRegister(tab[0], Boolean.parseBoolean(tab[2]), Integer.parseInt(tab[3]), Integer.parseInt(tab[4]), Integer.parseInt(tab[5]));
                }
            }
        }catch(FileNotFoundException e){
            e.printStackTrace();
        }catch (IOException e){
            e.printStackTrace();
        }

        /**
         * set the smartControl
         */
        Timer smartControlTimer = new Timer();
        smartControlTimer.scheduleAtFixedRate(new SmartControll(), 0,10000);

        /**
        * set the pollTimer
        */
        Timer timer = new Timer();
        timer.scheduleAtFixedRate(new PollTask(), 0,10000);



    }
}

