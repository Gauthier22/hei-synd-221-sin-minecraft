package ch.hevs.isi.core;

import ch.hevs.isi.database.DatabaseConntector;
import ch.hevs.isi.field.FieldConnector;
import ch.hevs.isi.web.WebConnector;
import com.serotonin.modbus4j.exception.ErrorResponseException;
import com.serotonin.modbus4j.exception.ModbusInitException;
import com.serotonin.modbus4j.exception.ModbusTransportException;

import java.io.IOException;

/**
 * This class modify the DataPoint class to work with boolean
 */

public class BinaryDataPoint extends DataPoint {
    boolean value;

    /**
     * Constructor for BinaryDataPoint
     * @param label
     * @param isOutput
     */
    public BinaryDataPoint(String label, boolean isOutput){
        super(label, isOutput);
    }

    /**
     * Update the values on the three connectors
     * @param newValue New value of the datapoint
     */
    public void setValue(boolean newValue) throws IOException {
            if (newValue != value) {
                this.value = newValue;
                DatabaseConntector.getInstance().onNewValue(this);
                WebConnector.getInstance().onNewValue(this);
                if (isOutput())
                    FieldConnector.getInstance().onNewValue(this);
            }

    }

    /**
     * Return the boolean value of the BinaryDataPoint
     */
    public boolean getValue(){
        return value ;
    }
}
