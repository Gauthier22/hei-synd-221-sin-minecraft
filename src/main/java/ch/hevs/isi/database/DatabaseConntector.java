package ch.hevs.isi.database;

import ch.hevs.isi.core.BinaryDataPoint;
import ch.hevs.isi.core.DataPoint;
import ch.hevs.isi.core.DataPointListener;
import ch.hevs.isi.core.FloatDataPoint;
import ch.hevs.isi.utils.Utility;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
/**
 *  For the backup and historic of all transaction.
 * implements DataPointListener
 */
public class DatabaseConntector implements DataPointListener {



    // actuellement hard codé mais après sera donné par les arguments du fichier jar
    private final static String url = "https://influx.sdi.hevs.ch/";
    private final static String dbName = "SIn14";
    private final static String user = "SIn14";
    private final static String password = "1ac5cd2b258e90009c69758c2da783df";
    String concactuserMdp = user + ":" + password;
    String encoding = java.util.Base64.getEncoder().encodeToString(concactuserMdp.getBytes());

    String type = "Binary/octet-stream";

    /**
     * Make the connection with the database
     * Singleton
     * Private attribute
     */
    private static DatabaseConntector dbc = null;

    // Private constructor
    private DatabaseConntector() {
    }

    // The static method getInstance() returns a reference to the singleton.
    // It creates the single X_Connector object if it does not exist.
    public static DatabaseConntector getInstance() {
        if (dbc == null) { dbc = new DatabaseConntector(); }
        return dbc;
    }


    /**
     * The method pushToDB make a new connection for each new value or change
     * @param label
     * @param value
     */
private void pushToDB(String label, String value)throws IOException{

    URL thisURL = new URL(url+"write?db="+dbName);
    HttpURLConnection connection = (HttpURLConnection) thisURL.openConnection();
    connection.setRequestProperty ("Authorization", "Basic " + encoding);
    connection.setRequestProperty("Content-Type", type);
    connection.setRequestMethod("POST");
    connection.setDoOutput(true);

    OutputStreamWriter writer = new OutputStreamWriter(connection.getOutputStream());
    System.out.println(label +  " value=" + value);
    writer.write( label + " value=" + value);
    writer.flush();

    int responseCode = connection.getResponseCode();
    System.out.println(responseCode);

    BufferedReader in = new BufferedReader(new InputStreamReader(connection.getInputStream()));
    if(responseCode == 204){
        while ( (in.readLine()) != null){

        }
    }
    connection.disconnect();
}



    /**
     * The method onNewValue make the update of the datapoint
     * @param fdp a float data point
     * @throws IOException
     */
    @Override
    public void onNewValue(FloatDataPoint fdp) throws IOException {
       pushToDB(fdp.getLabel(), String.valueOf(fdp.getValue()));
    }


    /**
     * The same method onNewValue for the binary datapoint
     * @param bdp a float data point
     * @throws IOException
     */
    public void onNewValue(BinaryDataPoint bdp) throws IOException {
        pushToDB(bdp.getLabel(), String.valueOf(bdp.getValue()));
    }
}
