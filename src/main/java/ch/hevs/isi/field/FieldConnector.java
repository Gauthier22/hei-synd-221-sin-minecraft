package ch.hevs.isi.field;

import ch.hevs.isi.core.*;
import ch.hevs.isi.utils.Utility;
import com.serotonin.modbus4j.exception.ErrorResponseException;
import com.serotonin.modbus4j.exception.ModbusInitException;
import com.serotonin.modbus4j.exception.ModbusTransportException;

import java.io.IOException;
import java.util.Timer;
import java.util.TimerTask;

public class FieldConnector{


    /**
     * Make the connection with the electric age minecraft
     * Singleton
     * Private attribute
     */
    private static FieldConnector fc = new FieldConnector();

    /**
     * Private constructor of FieldConnector
     */
    private FieldConnector() {
        //DNS / PORT / user /mdp etc pourront être ajouté à ce constructeur vide
    }

    /**
     * @param dp
     * @param isBoolean
     */
    private void pushToField(DataPoint dp, boolean isBoolean) throws ModbusTransportException, ErrorResponseException, ModbusInitException {
         try {
             if (isBoolean) {
                 BooleanRegister br = BooleanRegister.getRegisterFromDataPoint((BinaryDataPoint) dp);
                 System.out.println("Label : " + dp.getLabel() + " / Value : " + ((BinaryDataPoint) dp).getValue());
                 br.write();
             } else {
                 FloatRegister fr = FloatRegister.getRegisterFromDataPoint((FloatDataPoint) dp);
                 System.out.println("Label : " + dp.getLabel() + " / Value : " + ((FloatDataPoint) dp).getValue());
                 fr.write();
             }
         }catch(ModbusTransportException| ErrorResponseException| ModbusInitException e){
             e.printStackTrace();
             System.out.println("Exception has been found for the method 'pushToField, in FieldConnector'");
         }
    }

    /**
     * Ask the value and the label of the BinaryDataPoint to send it to the field
     * @param datapoint boolean datapoint to push on the field
     */
    public void onNewValue(BinaryDataPoint datapoint)  {
          try {
              //pushToField(datapoint, true);
              System.out.println("Label : " + datapoint.getLabel() + " / Value : " + datapoint.getValue());
              BooleanRegister br = BooleanRegister.getRegisterFromDataPoint(datapoint);
              br.write();
          }catch(ModbusTransportException| ErrorResponseException| ModbusInitException e){
              e.printStackTrace();
              System.out.println("Exception has been found for the method 'onNewValue (with BinaryDataPoint), in FieldConnector'");
          }
    }

    /**
     * Ask the value and the label of the FloatDataPoint to send it to the field
     * @param datapoint float datapoint to push on the field
     */
    public void onNewValue(FloatDataPoint datapoint)  {
        try {
            //pushToField(datapoint, false);
            System.out.println("Label : " + datapoint.getLabel() + " / Value : " + datapoint.getValue());
            FloatRegister fr = FloatRegister.getRegisterFromDataPoint(datapoint);
            fr.write();
        }catch(ModbusInitException e){
            e.printStackTrace();
            System.out.println("Exception has been found for the method 'onNewValue (with FloatDataPoint), in FieldConnector'");
        }
    }

    /**
     * Return the field connector
     * The static method getInstance() returns a reference to the singleton.
     * It creates the single X_Connector object if it does not exist.
     */
    public static FieldConnector getInstance(){
        return fc;
    }


    // rajouter un System print

    public static void main(String[] args) throws IOException, ModbusInitException, ModbusTransportException, ErrorResponseException {
       // FloatRegister a = new FloatRegister("REMOTE_COAL_SP", true, 209, 1);
       // BooleanRegister b = new BooleanRegister("REMOTE_SOLAR_SW", true, 401);
        // a.read();
       // b.read();

        BinaryDataPoint dp = (BinaryDataPoint) DataPoint.getDataPointFromLabel("REMOTE_SOLAR_SW");
        for (int i = 0; i < 5; i++) {
            dp.setValue(true);
            Utility.waitSomeTime(1000);
            dp.setValue(false);
            Utility.waitSomeTime(1000);
        }
    }
}
