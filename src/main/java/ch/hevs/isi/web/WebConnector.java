package ch.hevs.isi.web;

import ch.hevs.isi.core.BinaryDataPoint;
import ch.hevs.isi.core.DataPoint;
import ch.hevs.isi.core.DataPointListener;
import ch.hevs.isi.core.FloatDataPoint;
import ch.hevs.isi.database.DatabaseConntector;
import com.serotonin.modbus4j.exception.ErrorResponseException;
import com.serotonin.modbus4j.exception.ModbusInitException;
import com.serotonin.modbus4j.exception.ModbusTransportException;
import org.java_websocket.WebSocket;
import org.java_websocket.handshake.ClientHandshake;
import org.java_websocket.server.WebSocketServer;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.util.Vector;

public class WebConnector extends WebSocketServer implements DataPointListener {


    /**
     * Make the connection with the web visualisation
     * Singleton
     * Private attribute
     */
    private static WebConnector wbc = null;
    int range = 0; //variable bidon car le range c'est pour le modbus

    // Private constructor
    private WebConnector() {
        super(new InetSocketAddress(8888)); // the web page is the client and have this port hardcoded
        this.start();
    }

    Vector<WebSocket> vectorOfWebSocket = new Vector<WebSocket>();


    /**
     * .onOpen allow to start the web socket
     * @param webSocket is added in the vector who contain all websocket
     * @param clientHandshake
     */
    @Override
    public void onOpen(WebSocket webSocket, ClientHandshake clientHandshake) {
        webSocket.send("Welcome !!");
        vectorOfWebSocket.add(webSocket);
    }

    /**
     * the onClose method for the end of the socket
     * @param webSocket is removed of the vector
     * @param i
     * @param s
     * @param b
     */
    @Override
    public void onClose(WebSocket webSocket, int i, String s, boolean b) {
        webSocket.close();
        vectorOfWebSocket.remove(webSocket);
    }

    /**
     * the onMessage methode take the message and split it in a label and his value
     * @param webSocket
     * @param msg contain the label and the value and will be splited
     *            this method make the difference between boolean and float output
     */
    @Override
    public void onMessage(WebSocket webSocket, String msg) {
        String[] msgTab = msg.split("=");
        if(msgTab.length == 2){
            String label = msgTab[0];
            String value = msgTab[1];
            float f_value = Float.parseFloat(msgTab[1]);

            if(value == "true") {
                BinaryDataPoint bdp = new BinaryDataPoint(label, false);
                try {
                    bdp.setValue(true);
                } catch (IOException  e) {
                    e.printStackTrace();
                }
            }

            if(value == "false") {
                BinaryDataPoint bdp = new BinaryDataPoint(label, false);
                try {
                    bdp.setValue(false);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }


            FloatDataPoint fdp = new FloatDataPoint(label, false, range);

            try {
                fdp.setValue(f_value);
            } catch (IOException  e) {
                e.printStackTrace();
            }

            pushToWeb(label, value);

        }else{
            System.out.println("error onMessage it doesn't contain : ''label'' = ''value'' ");
        }
    }

    @Override
    public void onError(WebSocket webSocket, Exception e) {
        //Not need
    }

    @Override
    public void onStart() {
        //Not need
    }


    /**
     * The static method getInstance() returns a reference to the singleton.
     * It creates the single X_Connector object if it does not exist.
     * @return a web connector
     */

    public static WebConnector getInstance() {
        if (wbc == null) { wbc = new WebConnector(); }
        return wbc;
    }

    /**
     * The onNewValue method make the update of the datapoint
     * @param bdp BinaryDataPoint
     */
    public void onNewValue(BinaryDataPoint bdp) {
        pushToWeb(bdp.getLabel(), String.valueOf(bdp.getValue()));
    }

    /**
     * The same method for the float datapoint
     * @param fdp
     */
    public void onNewValue(FloatDataPoint fdp) {
        pushToWeb(fdp.getLabel(), String.valueOf(fdp.getValue()));

    }


    /**
     * The pushToWeb method update data in the web visualisation
     * @param label
     * @param value
     * The 2 param ar concatenated and send
     */
    private void pushToWeb(String label, String value) {
        System.out.println("Push to web: " + label + " value=" + value);
        // est ce qu'il faut faire un "read" depuis la visu

        for(WebSocket ws : vectorOfWebSocket){ //itération
            ws.send(label + "=" + value);

        }
    }


}
