﻿# Minecraft electrical age project
## Introduction

Welcome to our modbus and websockets communication project.
The goal of this project is to manage a factory simulated in a Minecraft world. We had to developp a software for the industrial automatisation of the mode "Electrical Age" of Minecraft. Nevertheless, our programme doesn't depend on Minecraft, it can as well being used for piloting any other PLC programmable.

## Function
Our program is filling a database that we need to reed all the informations about the virtual world on a web page. Grafana is the webtool to visualize all the data on time. 
The program is communicating all parameters stocked on a CSV. file to a component called "Field". That component is the link between an interface and the virtual world by Modbus. All of these functions are managed in this component. The CSV file content all the useful data to control the Minecraft world (label, address, ...). This has to be updated if the system we want to control is different.
The second part which is necessary to read the data on a web page is the link between the database and the web. The "framework" links the database, the interface web, the field and a control instance "SmartControll.
So when any value of data is changing in the system all of these component are going to be updated. But when the control instance decide to change something to manage the regulation of the system, the database is updated and the parameters of the system are updated. SmartControll is the only component which is specific to this system, the others can be reused.

## Javadoc
All the documentation about the classes and the programmation is available in the field "javadoc" present in the source of the project. Click on the web index and the Javadoc appear.

## .jar
The file to execute the programme is available in the field "out" in the source of the project. "Minecraft.jar" is the executable of the project, but it is useful only with the Minecraft World or a PLC system and the web page of Grafana.

## Credits
Devellopers Sierro Nathan and Tschopp Gauthier.


